sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend("ns.Partner.controller.Worklist", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
        onInit: function () {
            //Create JSON Model with URL
            var oModel = new sap.ui.model.json.JSONModel();

            //API Key for API Sandbox
            var sHeaders = { "Content-Type": "application/json", "Accept": "application/json", "APIKey": "lu2U8cedqG2GZypPOL8Jj9a1GhNQT5r9" };

            //Available Security Schemes for productive API Endpoints
            //Basic Authentication

            //Basic Auth : provide username:password in Base64 encoded in Authorization header

            //sending request
            //API endpoint for API sandbox 
            oModel.loadData("https://sandbox.api.sap.com/successfactors/odata/v2/User", null, true, "GET", null, false, sHeaders);
            this.getView().setModel(oModel, "results");
        },

        onPressedLine: function (oEvent) {
            this._showObject(oEvent.getSource());

        },
        _showObject: function (oItem) {
            debugger;
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext("results").getProperty("userId") 
                
            });
        },



    });
});